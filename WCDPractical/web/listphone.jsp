
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>List phone</title>
</head>
<body>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>List of Phones</h2></caption>
        <tr>
            <th>Name</th>
            <th>Brand</th>
            <th>Price</th>
            <th>Description</th>
        </tr>
        <c:forEach var="phone" items="${phoneList}">
            <tr>
                <td><c:out value="${phone.name}"/></td>
                <td><c:out value="${phone.brand}"/></td>
                <td><c:out value="${phone.price}"/></td>
                <td><c:out value="${phone.description}"/></td>
            </tr>
        </c:forEach>
    </table>
    <a href="/WCDPractical_war_exploded">Back</a>
</div>
</body>
</html>
