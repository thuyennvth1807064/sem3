﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observable_Collection.Model
{
   public class Icon
    {
        public string IconPath { get; set; }
    }
}
