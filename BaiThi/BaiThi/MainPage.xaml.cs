﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace BaiThi
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            Output.ItemsSource = Grab_Entries();
        }

        public void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboBoxResultTextBlock == null) return;
            var combo = (ComboBox)sender;
            var item = (ComboBoxItem)combo.SelectedItem;
            ComboBoxResultTextBlock.Text = item.Content.ToString();
        }

        public void YesRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            string myRadioButton = (bool)MaleRadioButton.IsChecked ? "Male" : "Female";
        }

        public void MyCheckBox_Tapped(object sender, TappedRoutedEventArgs e)
        {
            string myCheckBox = MyCheckBox.IsChecked.ToString();
            myCheckBox += MyCheckBox2.IsChecked.ToString();
        }

        private void MyButton_Click(object sender, RoutedEventArgs e)
        {
            using (SqliteConnection db = new SqliteConnection("Filename=sqliteSampleDemo.db"))
            {
                db.Open();

                // init db
                String tableCommand = "CREATE TABLE IF NOT " +
                                        "EXISTS MyTable (Primary_Key INTEGER PRIMARY KEY, " +
                                        "UserName NVARCHAR(2048) NULL)";

                SqliteCommand createTable = new SqliteCommand(tableCommand, db);

                createTable.ExecuteReader();

                // add entries
                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;
                insertCommand.CommandText = "INSERT INTO MyTable VALUES (NULL,@Entry)";
                insertCommand.Parameters.AddWithValue("@Entry", ResultUserNameTextBox.Text);
                try
                {
                    insertCommand.ExecuteReader();
                }
                catch (SqliteException error)
                {
                    return;
                }
                db.Close();
            }
            Output.ItemsSource = Grab_Entries();
        }

        private List<String> Grab_Entries()
        {
            List<String> entries = new List<string>();
            using (SqliteConnection db = new SqliteConnection("Filename=sqliteSampleDemo.db"))
            {
                db.Open();
                SqliteCommand selectCommand =
                 new SqliteCommand("SELECT UserName from MyTable", db);
                SqliteDataReader query;
                try
                {
                    query = selectCommand.ExecuteReader();
                }
                catch (SqliteException e)
                {
                    return entries;
                }
                while (query.Read())
                {
                    entries.Add(query.GetString(0));
                }
                db.Close();
            }
            return entries;
        }
    }
}