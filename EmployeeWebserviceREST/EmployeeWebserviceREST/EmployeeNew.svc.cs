﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace EmployeeWebserviceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeNew : IEployee
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();

        public bool AddEmployee(Employee1 emp1)
        {
            try
            {
                data.Employee1s.InsertOnSubmit(emp1);
                data.SubmitChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployee(int idE)
        {
            try
            {
                Employee1 employeeToDelete = 
                    (from employee1 in data.Employee1s where employee1.EmployeeID == idE select employee1).Single();
                data.Employee1s.DeleteOnSubmit(employeeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //public string GetData(int value)
        //{
        //    return string.Format("You entered: {0}", value);
        //}

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}

        public List<Employee1> GetProductList()
        {
            try
            {
                return (from employee1 in data.Employee1s select employee1).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmployee(Employee1 eml)
        {
            Employee1 employeeToModify =
                (from a in data.Employee1s where a.EmployeeID == eml.EmployeeID select a).Single();
            employeeToModify.Age = eml.Age;
            employeeToModify.address = eml.address;
            employeeToModify.firstName = eml.firstName;
            employeeToModify.lastName = eml.lastName;
            data.SubmitChanges();
            return true;
        }
    }
}
