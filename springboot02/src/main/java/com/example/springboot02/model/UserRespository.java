package com.example.springboot02.model;

import com.example.springboot02.entity.User;
import org.springframework.data.repository.CrudRepository;
//JPA
public interface UserRespository extends CrudRepository <User,Long> {

}
