package com.example.springboot02.controller;

import com.example.springboot02.entity.User;
import com.example.springboot02.model.UserRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class UserController {

    //DI call medel
    @Autowired
    UserRespository userRespository;
    //Step1
    @RequestMapping("/")
    public String index(Model model)
    {
        //Call biz - Step2
        List<User> users =(List<User>) userRespository.findAll();
        //step3
        //request.setAttribute("users", users) : or session.setAttribute("users",users);
        model.addAttribute("users",users);
        return "index";
    }
    @RequestMapping(value = "add")
    public String addUser(Model model)
    {
        model.addAttribute("user",new User());
        return "addUser";
    }
    //Step1
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public String save(User user)
    {
        //Step2
        userRespository.save(user);
        return "redirect:/";
    }

    @RequestMapping(value = "/edit",method = RequestMethod.GET)
    public String editUser(@RequestParam("id") Long userId,Model model)
    {
        Optional<User> userEdit= userRespository.findById(userId);
        userEdit.ifPresent(user -> model.addAttribute("user",user));
        return "editUser";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam("id")Long userId, Model model)
    {
        userRespository.deleteById(userId);
        return "redirect:/";
    }

}
