package com.example.springboot02.controller;

import com.example.springboot02.entity.Product;
import com.example.springboot02.model.ProductRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class ProductController {

    //DI call medel
    @Autowired
    ProductRespository productRespository;
    //Step1
    @RequestMapping("/indexProduct")
    public String indexProduct(Model model)
    {
        //Call biz - Step2
        List<Product> products =(List<Product>) productRespository.findAll();
        //step3
        model.addAttribute("products",products);
        return "/indexProduct";
    }
    @RequestMapping(value = "product/addProduct")
    public String addProduct(Model model)
    {
        model.addAttribute("product",new Product());
        return "addProduct";
    }
    //Step1
    @RequestMapping(value = "/product/saveProduct",method = RequestMethod.POST)
    public String saveProduct(Product product)
    {
        //Step2
        productRespository.save(product);
        return "redirect:/indexProduct";
    }

    @RequestMapping(value = "/product/editProduct",method = RequestMethod.GET)
    public String editProduct(@RequestParam("id") Long productId,Model model)
    {
        Optional<Product> productEdit= productRespository.findById(productId);
        productEdit.ifPresent(product -> model.addAttribute("product",product));
        return "editProduct";
    }
    @RequestMapping(value = "/product/deleteProduct", method = RequestMethod.GET)
    public String deleteProduct(@RequestParam("id")Long productId, Model model)
    {
        productRespository.deleteById(productId);
        return "indexProduct";
    }

}
