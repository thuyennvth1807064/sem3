<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Login Page</title>
    <style>
        * {
            box-sizing: border-box;
            margin: 0;
        }

        .login_form {
            width: 100vw;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        .box {
            margin: 10px 0;
        }

        .error-message {
            color: red;
        }

        input[type=submit].box {
            width: 100%;
        }
    </style>
</head>
<body>
<div class="login_form">
    <h1 class="box">Login</h1>
    <p class="error-message"><c:out value="${message}"/></p>
    <form action="LoginServlet" method="post">
        <input class="box" type="text" name="email" placeholder="Email"><br/>
        <input class="box" type="password" name="password" placeholder="Password"><br/>
        <input class="box" type="submit" value="Login"><br/>
    </form>
</div>
</body>
</html>
