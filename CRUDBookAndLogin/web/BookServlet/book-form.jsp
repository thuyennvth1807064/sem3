<%--
  Created by IntelliJ IDEA.
  User: Phạm Dũng
  Date: 4/30/2020
  Time: 6:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Book Form</title>
</head>
<body>
<div style="text-align: center;">
    <h1>Books Management</h1>
    <h2>Manager: dungpham1218@gmail.com</h2>
    <h2>
        <a href="new">Add New Book</a>
        <a href="list">List All Books</a>
    </h2>
</div>
<div align="center">
    <c:if test="${book != null}">
    <form action="update" method="post">
        </c:if>
        <c:if test="${book == null}">
        <form action="insert" method="post">
            </c:if>
            <table border="1" cellpadding="5">
                <caption>
                    <h2>
                        <c:if test="${book != null}">Edit model.Book</c:if>
                        <c:if test="${book == null}">Add New model.Book</c:if>
                    </h2>
                </caption>
                <c:if test="${book != null}">
                    <input type="hidden" name="id" value="<c:out value='${book.id}' />"/>
                </c:if>
                <tr>
                    <th>Title:</th>
                    <td>
                        <input type="text" name="title" size="45" value="<c:out value='${book.title}' />"/>
                    </td>
                </tr>
                <tr>
                    <th>Author:</th>
                    <td>
                        <input type="text" name="author" size="45" value="<c:out value='${book.author}' />"/>
                    </td>
                </tr>
                <tr>
                    <th>Price:</th>
                    <td>
                        <input type="text" name="price" size="5" value="<c:out value='${book.price}' />"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Save"/>
                    </td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>
