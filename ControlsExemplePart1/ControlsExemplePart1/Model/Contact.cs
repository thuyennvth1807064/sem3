﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlsExemplePart1.Model
{
    public class Contact
    {
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string AvatarPath { get; set; }
    }
}
